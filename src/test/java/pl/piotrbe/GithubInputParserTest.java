package pl.piotrbe;

import org.junit.Assert;
import org.junit.Test;
import pl.piotrbe.client.LastCommitsShaParser;
import pl.piotrbe.client.github.GithubClient;
import pl.piotrbe.client.LastCommitShaClient;
import pl.piotrbe.client.github.GithubLastCommitParser;
import pl.piotrbe.input.InputParser;


public class GithubInputParserTest {
    public LastCommitsShaParser getParser(String s) {
        return new GithubLastCommitParser(s);
    }

    @Test
    public void testIfOptionsAreParsed() {
        String exampleJson1 = "[{\"sha\":\"123\"},{\"sha\":\"abc\"}]";
        LastCommitsShaParser parser = getParser(exampleJson1);

        Assert.assertEquals("123", parser.parse());

        String exampleJson2 = "[{\"sha\":\"222123\"},{\"sha\":\"abc\"}]";
        LastCommitsShaParser parser2 = getParser(exampleJson2);

        Assert.assertEquals("222123", parser2.parse());
    }
}
