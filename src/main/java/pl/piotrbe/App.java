package pl.piotrbe;

import pl.piotrbe.client.github.GithubClient;
import pl.piotrbe.client.LastCommitShaClient;
import pl.piotrbe.client.github.GithubLastCommitParser;
import pl.piotrbe.input.InputParser;

public class App {
    public static void main(String[] args) {
        //FILL IN WITH PUBLIC GITHUB REPOSITORY DATA
        String[] args2 = {"owner/repo"};
        String sha = App.run(args2);
        System.out.println(sha);
    }

    private static String run(String[] args) {
        InputParser inputParser = new InputParser();
        String[] arguments = inputParser.getParsedArguments(args);

        //add --service here
        LastCommitShaClient client = new GithubClient();
        String content = client.fetchLastSha(arguments[0]);
        GithubLastCommitParser parser = new GithubLastCommitParser(content);

        return parser.parse();
    }
}
