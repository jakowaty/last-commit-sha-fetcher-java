package pl.piotrbe.input;

import org.apache.commons.cli.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class InputParser {
    private Options options;

    public InputParser () {
        this.options = new Options();
    }

    public Options getParsedOptions() {
        return options;
    }

    public void addOption(String shortcut, String longOption, boolean hasArgument, String description, boolean required) {
        Option option = new Option(shortcut, longOption, hasArgument, description);

        option.setRequired(required);
        options.addOption(option);
    }

    private CommandLine parse(Options options, String[] fixedArgs) {
        DefaultParser commandLineParser = new DefaultParser();

        try {
            return commandLineParser.parse(options, fixedArgs);
        } catch (Exception e) {}

        return null;
    }

    public String[] getParsedArguments(String[] input) {
        //use optionals
        CommandLine commandLine = parse(this.options, input);

        if (commandLine == null) {
            return null;
        }

        return commandLine.getArgs();
    }

    public Map<String,String> getParsedOptions(String[] input) {
        //use optionals
        CommandLine commandLine = parse(this.options, input);

        if (commandLine == null) {
            return null;
        }

        Option[] opts = commandLine.getOptions();
        Map<String,String> result = new HashMap<>();

        for (Option o : opts) {
            if (o.hasLongOpt()) {
                result.put(o.getLongOpt(), o.getValue());
            }

            result.put(o.getOpt(),o.getValue());
        }

        return result;
    }
}
