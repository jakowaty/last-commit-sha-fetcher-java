package pl.piotrbe.client;

public interface LastCommitsShaParser {
    public String parse();
}
