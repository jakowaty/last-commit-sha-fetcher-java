package pl.piotrbe.client;

public enum ClientType {
    BITBUCKET("bitbucket"),GITHUB("github");

    private String name;

    private ClientType(String name) {
        this.name = name.toLowerCase();
    }

    public String getName() {
        return name;
    }
}
