package pl.piotrbe.client.github;

import org.json.JSONArray;
import pl.piotrbe.client.LastCommitsShaParser;

public class GithubLastCommitParser implements LastCommitsShaParser {
    private String json;

    public GithubLastCommitParser(String jsonStr) {
        json = jsonStr;
    }

    @Override
    public String parse() {
        JSONArray jsonObject = new JSONArray(json);
        return jsonObject.getJSONObject(0).getString("sha");
    }
}
