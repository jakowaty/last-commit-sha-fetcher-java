package pl.piotrbe.client.github;

import pl.piotrbe.client.LastCommitShaClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class GithubClient implements LastCommitShaClient {
    public static final String apiURL = "https://api.github.com/repos/";

    @Override
    public String fetchLastSha(String ownerRepo) {
        BufferedReader reader;
        StringBuilder finalUrl = new StringBuilder(apiURL);

        finalUrl.append(ownerRepo);
        finalUrl.append("/commits");

        reader = createConnection(createURL(finalUrl.toString()));

        return this.read(reader);
    }

    private String read(BufferedReader bufferedReader) {
        StringBuilder inputLine = new StringBuilder();
        String inputLineCurrent;

        try {
            while ((inputLineCurrent = bufferedReader.readLine()) != null) {
                inputLine.append(inputLineCurrent);
            }

            bufferedReader.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return  inputLine.toString();
    }

    private BufferedReader createConnection(URL url) {
        if (url == null) {
            return null;
        }

        try {
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(url.openStream())
            );

            return reader;
        } catch (IOException ioex) {
            System.out.println(ioex.getMessage());
            ioex.printStackTrace();
        }

        return null;
    }

    private URL createURL(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException muex) {}

        return null;
    }

}
