package pl.piotrbe.client.github;

import pl.piotrbe.client.ClientType;
import pl.piotrbe.client.LastCommitShaClient;

public class LastCommitShaClientFactory {
    public static LastCommitShaClient createClient(ClientType type) {
        if (type.equals(ClientType.GITHUB)) {
            return new GithubClient();
        }

        return null;
    }
}
