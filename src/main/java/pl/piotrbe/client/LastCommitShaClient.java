package pl.piotrbe.client;

public interface LastCommitShaClient {
    public String fetchLastSha(String ownerRepo);
}
